#!/usr/bin/env bash

CURRENT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
compile=""
biberarg=""
CMD_LATEX=lualatex

compile='$CMD_LATEX --shell-escape "./cv.tex"'
biberarg="./cv"

eval "$compile"
RETVAL="$?"
if [[ "${RETVAL}" -ne 0 ]] ; then
    echo "First $CMD_LATEX run failed"
    exit ${RETVAL}
fi

biber "$biberarg"
RETVAL="$?"
if [[ "${RETVAL}" -ne 0 ]] ; then
    echo "biber run failed"
    exit ${RETVAL}
fi

eval "$compile"
RETVAL="$?"
if [[ "${RETVAL}" -ne 0 ]] ; then
    echo "Second $CMD_LATEX run failed"
    exit ${RETVAL}
fi

eval "$compile"
RETVAL="$?"
if [[ "${RETVAL}" -ne 0 ]] ; then
    echo "Third $CMD_LATEX run failed"
    exit ${RETVAL}
fi

rm *.bbl 2> /dev/null
rm *.blg 2> /dev/null
rm *.aux 2> /dev/null
rm *.bcf 2> /dev/null
rm *.ilg 2> /dev/null
rm *.lof 2> /dev/null
rm *.log 2> /dev/null
rm *.lot 2> /dev/null
rm *.nlo 2> /dev/null
rm *.nls* 2> /dev/null
rm *.out 2> /dev/null
rm *.toc 2> /dev/null
rm *.run.xml 2> /dev/null
rm *.lot 2> /dev/null

echo "PDF Compile: Success"

exit 0
